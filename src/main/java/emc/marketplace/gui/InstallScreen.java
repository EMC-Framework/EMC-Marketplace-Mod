package emc.marketplace.gui;

import emc.marketplace.api.install.FabricModMan;
import emc.marketplace.main.Main;
import emc.marketplace.api.SerializedMod;
import me.deftware.client.framework.chat.LiteralChatMessage;
import me.deftware.client.framework.fonts.minecraft.FontRenderer;
import me.deftware.client.framework.gui.GuiScreen;
import me.deftware.client.framework.gui.widgets.Button;
import me.deftware.client.framework.minecraft.Minecraft;

public class InstallScreen extends GuiScreen {

	private final SerializedMod mod;
	private int counter = 0;

	public InstallScreen(GuiScreen parent, SerializedMod mod) {
		super(parent);
		this.mod = mod;
		Main.status = "Installing mod...";
		if (!(mod.getConflicts() != null && !mod.getConflicts().isEmpty())) {
			mod.install((status) ->
				Main.status = status ? "Done, mod has been installed" : "Install failed! Check logs for more info"
			);
		}
	}

	@Override
	protected void onInitGui() {
		this.clearButtons();
		this.clearTexts();
		this.addButton(new Button(0, this.getGuiScreenWidth() / 2 - 155, this.getGuiScreenHeight() / 6 + 96, 310, 20, new LiteralChatMessage("Back")) {
			@Override
			public void onButtonClick(double mouseX, double mouseY) {
				Minecraft.openScreen(parent);
			}
		});
		getIButtonList().get(0).setEnabled(false);
		this.addCenteredText(this.getGuiScreenWidth() / 2, 40, new LiteralChatMessage(String.format("Installing %s", mod.getName())));
		this.addCenteredText(getGuiScreenWidth() / 2, 90, new LiteralChatMessage("Status:"));

	}

	@Override
	protected void onDraw(int mouseX, int mouseY, float delta) {
		// Check for conflicts
		if (mod.getConflicts() != null && !mod.getConflicts().isEmpty()) {
			String[] conflicts = new String[mod.getConflicts().size() + 1];
			conflicts[0] = mod.getName() + " cannot be installed because it has conflicts with:";
			for (int i = 1; i < mod.getConflicts().size() + 1; i++) {
				conflicts[i] = mod.getConflicts().get(i - 1);
			}
			Minecraft.openScreen(new WarningScreen(parent, "Cannot install due to conflicts!", cb ->
				Minecraft.openScreen(parent)
			, conflicts));
			return;
		}
		// Draw
		this.renderBackgroundTextureWrap(0);
		FontRenderer.drawCenteredString(new LiteralChatMessage(Main.status), getGuiScreenWidth() / 2, 100, 0xFFFFFF);
		if (getIButtonList().get(0).isEnabled() && mod.getModMan() instanceof FabricModMan) {
			FontRenderer.drawCenteredString(new LiteralChatMessage("You must restart Minecraft in order for the mod to work"), getGuiScreenWidth() / 2, 100 + FontRenderer.getFontHeight() + 2, 0xFFFFFF);
		}
	}

	@Override
	protected void onUpdate() {
		if (Main.status.toLowerCase().contains("done") || Main.status.toLowerCase().contains("restart") || Main.status.toLowerCase().contains("failed")) {
			counter++;
		}
		if (counter >= 30) {
			getIButtonList().get(0).setEnabled(true);
		}
	}

}
