package emc.marketplace.gui;

import emc.marketplace.api.SerializedMod;
import lombok.Getter;
import me.deftware.client.framework.chat.LiteralChatMessage;
import me.deftware.client.framework.fonts.minecraft.FontRenderer;
import me.deftware.client.framework.gui.GuiScreen;
import me.deftware.client.framework.gui.widgets.Button;
import me.deftware.client.framework.minecraft.Minecraft;
import org.lwjgl.glfw.GLFW;

public class ModInfoScreen extends GuiScreen {

	private @Getter final SerializedMod mod;

	ModInfoScreen(SerializedMod mod, GuiScreen parent) {
		super(parent);
		this.mod = mod;
	}

	@Override
	protected void onInitGui() {
		this.clearButtons();
		this.clearTexts();
		this.addButton(new Button(0, getGuiScreenWidth() / 2 - 100, getGuiScreenHeight() - 28, 98, 20,
				new LiteralChatMessage(mod.isInstalled() ? "Uninstall" : "Install")) {
			@Override
			public void onButtonClick(double v, double v1) {
				if (mod.isInstalled()) {
					mod.uninstall();
				} else {
					if (mod.isBeta()) {
						Minecraft.openScreen(new ConfirmScreen(ModInfoScreen.this, "Warning! Beta software:", (status) -> {
							if (status) {
								Minecraft.openScreen(new InstallScreen(ModInfoScreen.this, mod));
							}
						}, mod.getName() + " is in beta right now,", "installing it may cause instability and/or render problems.", "Install at your own risk, do not report bugs from this addon."));
					} else {
						Minecraft.openScreen(new InstallScreen(ModInfoScreen.this, mod));
					}
				}
			}
		});
		this.addButton(new Button(1, getGuiScreenWidth() / 2 + 2, getGuiScreenHeight() - 28, 98, 20, new LiteralChatMessage("Back")) {
			@Override
			public void onButtonClick(double v, double v1) {
				Minecraft.openScreen(new ModListScreen(parent));
			}
		});
		addCenteredText(getGuiScreenWidth() / 2, 8, new LiteralChatMessage(mod.getName()));
		addCenteredText(getGuiScreenWidth() / 2, 20, new LiteralChatMessage(String.format("Developed by: %s", mod.getAuthor())));
		addCenteredText(getGuiScreenWidth() / 2, 60, new LiteralChatMessage("Mod description:"));
		int y = 70;
		for (String string : mod.getDescription()) {
			addCenteredText(getGuiScreenWidth() / 2, y, new LiteralChatMessage(string));
			y += FontRenderer.getFontHeight() + 2;
		}
	}

	@Override
	protected void onDraw(int mouseX, int mouseY, float partialTicks) {
		this.renderBackgroundTextureWrap(0);
	}

}
