package emc.marketplace.gui;

import emc.marketplace.api.MarketplaceAPI;
import emc.marketplace.api.SerializedMod;
import emc.marketplace.main.Main;
import me.deftware.client.framework.FrameworkConstants;
import me.deftware.client.framework.chat.LiteralChatMessage;
import me.deftware.client.framework.chat.builder.ChatBuilder;
import me.deftware.client.framework.fonts.minecraft.FontRenderer;
import me.deftware.client.framework.gui.GuiScreen;
import me.deftware.client.framework.gui.minecraft.GuiSlot;
import me.deftware.client.framework.gui.widgets.Button;
import me.deftware.client.framework.input.Keyboard;
import me.deftware.client.framework.item.ItemStack;
import me.deftware.client.framework.minecraft.Minecraft;
import me.deftware.client.framework.registry.BlockRegistry;
import me.deftware.client.framework.util.path.LocationUtil;
import org.lwjgl.opengl.GL11;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ModListScreen extends GuiScreen {

	private ModList modList;

	public ModListScreen(GuiScreen parent) {
		super(parent);
	}

	@Override
	protected void onInitGui() {
		Main.status = "";
		modList = new ModList(this);
		this.addEventListener(modList);
		modList.clickElement(-1, false, 0, 0);
		this.clearButtons();
		this.clearTexts();
		this.addButton(new Button(0, getGuiScreenWidth() / 2 - 100, getGuiScreenHeight() - 28, 200, 20, new LiteralChatMessage("Back")) {
			@Override
			public void onButtonClick(double v, double v1) {
				Minecraft.openScreen(parent);
			}
		});
		this.addButton(
				new Button(1, getGuiScreenWidth() / 2 - 100, getGuiScreenHeight() - 51, 98, 20, new LiteralChatMessage("Install")) {
					@Override
					public void onButtonClick(double v, double v1) {
						if (MarketplaceAPI.getMods() != null) {
							SerializedMod selected = (SerializedMod) MarketplaceAPI.getMods().toArray()[modList.getSelectedSlot()];
							if (selected.isInstalled()) {
								selected.uninstall();
							} else {
								if (selected.isBeta()) {
									Minecraft.openScreen(new ConfirmScreen(ModListScreen.this, "Warning! Beta software:", (status) -> {
										if (status) {
											Minecraft.openScreen(new InstallScreen(ModListScreen.this, selected));
										}
									}, selected.getName() + " is in beta right now,", "installing it may cause instability and/or render problems.", "Install at your own risk, do not report bugs from this addon."));
								} else {
									Minecraft.openScreen(new InstallScreen(ModListScreen.this, selected));
								}
							}
						}
					}
				});
		this.addButton(
				new Button(2, getGuiScreenWidth() / 2 + 2, getGuiScreenHeight() - 51, 98, 20, new LiteralChatMessage("Details...")) {
					@Override
					public void onButtonClick(double v, double v1) {
						if (MarketplaceAPI.getMods() != null) {
							// We can assume the selected is not null, since you cannot press any of the
							// buttons if no mod is selected
							SerializedMod selected = (SerializedMod) MarketplaceAPI.getMods().toArray()[modList.getSelectedSlot()];
							// More info
							Minecraft.openScreen(new ModInfoScreen(selected, parent));
						}
					}
				});
		// Add mod dir button
		boolean fabric = FrameworkConstants.MAPPING_LOADER == FrameworkConstants.MappingsLoader.Fabric, forge = FrameworkConstants.MAPPING_LOADER == FrameworkConstants.MappingsLoader.Forge;
        if (fabric || forge) {
			this.addButton(
					new Button(3, 8, getGuiScreenHeight() - 28, 98, 20, new LiteralChatMessage((fabric ? "Fabric" : "Forge") + " mods")) {
						@Override
						public void onButtonClick(double v, double v1) {
							try {
								Keyboard.openLink(new File(fabric ?
										System.getProperty("EMCDir", LocationUtil.getEMC().toFile().getParentFile().getAbsolutePath()) + File.separator :
										Minecraft.getRunDir().getAbsolutePath() + File.separator + "mods" + File.separator).toURI().toString());
							} catch (Exception ex) {
								ex.printStackTrace();
							}
						}
					});
		}
		getIButtonList().get(1).setEnabled(false);
		getIButtonList().get(2).setEnabled(false);
		addCenteredText(getGuiScreenWidth() / 2, 8, new LiteralChatMessage("Addons Marketplace"));
		addText(2, 2, new LiteralChatMessage("Marketplace v" + Main.version));
	}

	@Override
	protected void onDraw(int mouseX, int mouseY, float partialTicks) {
		this.renderBackgroundTextureWrap(0);
		modList.doDraw(mouseX, mouseY, partialTicks);
		FontRenderer.drawCenteredString(new LiteralChatMessage((MarketplaceAPI.getMods() != null ? MarketplaceAPI.getTotalMods() : "0") + " mods available"), getGuiScreenWidth() / 2, 20, 16777215);
		if (MarketplaceAPI.getMods() == null || modList.getISize() == 0) {
			FontRenderer.drawCenteredString(new LiteralChatMessage("Loading mods... "), getGuiScreenWidth() / 2, 45, 16777215);
		}
	}

	@Override
	protected void onUpdate() {
		if (MarketplaceAPI.getMods() == null) {
			return;
		}
		SerializedMod selected = null;
		if (MarketplaceAPI.getMods() != null && !(MarketplaceAPI.getTotalMods() == 0) && modList.getSelectedSlot() != -1) {
			selected = (SerializedMod) MarketplaceAPI.getMods().toArray()[modList.getSelectedSlot()];
		}
		if (selected != null) {
			getIButtonList().get(1).setEnabled(true);
			getIButtonList().get(2).setEnabled(true);
			getIButtonList().get(1).setButtonText(new LiteralChatMessage(selected.isInstalled() ? "Uninstall" : "Install"));
		} else {
			getIButtonList().get(1).setEnabled(false);
			getIButtonList().get(2).setEnabled(false);
			getIButtonList().get(1).setButtonText(new LiteralChatMessage("Install"));
		}
	}

	private static class ModList extends GuiSlot {

		private final ItemStack emerald, diamond;

		ModList(GuiScreen parent) {
			super(parent.getGuiScreenWidth(), parent.getGuiScreenHeight(), 36, parent.getGuiScreenHeight() - 56, 54);
			this.emerald = new ItemStack(
					BlockRegistry.INSTANCE.find("emerald_block").orElseThrow(() -> new NullPointerException("Failed to find block emerald_block"))
					, 1);
			this.diamond = new ItemStack(
					BlockRegistry.INSTANCE.find("diamond_block").orElseThrow(() -> new NullPointerException("Failed to find block diamond_block"))
					, 1);
		}

		@Override
		protected int getISize() {
			return MarketplaceAPI.getMods() != null ? MarketplaceAPI.getTotalMods() : 0;
		}

		@Override
		protected void drawISlot(int id, int x, int y) {
			if (MarketplaceAPI.getMods() == null) {
				return;
			}
			SerializedMod mod = (SerializedMod) MarketplaceAPI.getMods().toArray()[id];

			ItemStack icon = mod.isInstalled() ? emerald : diamond;
			try {
				GL11.glEnable(GL11.GL_DEPTH_TEST);
				icon.renderItemAndEffectIntoGUI(x + 4, y + 4);
				icon.renderItemOverlays(x + 4, y + 4);
				GL11.glDisable(GL11.GL_DEPTH_TEST);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			FontRenderer.drawString(new ChatBuilder().withText(mod.getName()).setBold().append().withSpace()
					.withText(String.format("By %s", mod.getAuthor())).setItalic().build(), x + 31, y + 3, 10526880);

			// Description
			int maxWidth = 180;
			List<String> description = new ArrayList<>();
			if (!mod.getSummary().contains(" ")) {
				description.add(mod.getSummary());
			} else {
				String current = "";
				for (String word : mod.getSummary().split(" ")) {
					if (current.isEmpty()) {
						current = word;
					} else {
						if (FontRenderer.getStringWidth(current + " " + word) > maxWidth) {
							description.add(current);
							current = word;
						} else {
							current += " " + word;
						}
					}
				}
				description.add(current);
			}
			y += 15;
			x += 31;
			for (int i = 0; i < description.size(); i++) {
				if (i < 3) {
					FontRenderer.drawString(new LiteralChatMessage(description.get(i)), x, y, 10526880);
					y += 12;
				}
			}
		}

	}

}
