package emc.marketplace.gui;

import me.deftware.client.framework.chat.ChatMessage;
import me.deftware.client.framework.chat.LiteralChatMessage;
import me.deftware.client.framework.fonts.minecraft.FontRenderer;
import me.deftware.client.framework.gui.GuiScreen;
import me.deftware.client.framework.gui.widgets.Button;
import me.deftware.client.framework.minecraft.Minecraft;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author Deftware
 */
public class ConfirmScreen extends GuiScreen {

	protected ChatMessage title;
	protected List<ChatMessage> text = new ArrayList<>();
	protected Consumer<Boolean> onPressed;

	public ConfirmScreen(GuiScreen parent, String title, Consumer<Boolean> onPressed, String... text) {
		super(parent);
		this.title = new LiteralChatMessage(title);
		for (String line : text) this.text.add(new LiteralChatMessage(line));
		this.onPressed = onPressed;
	}

	@Override
	protected void onInitGui() {
		this.clearButtons();
		this.addButton(new Button(0, this.getGuiScreenWidth() / 2 - 155, this.getGuiScreenHeight() / 6 + 96, 150, 20, new LiteralChatMessage("Continue")) {
			@Override
			public void onButtonClick(double mouseX, double mouseY) {
				Minecraft.openScreen(parent);
				onPressed.accept(true);
			}
		});
		this.addButton(new Button(1, this.getGuiScreenWidth() / 2 - 155 + 160, this.getGuiScreenHeight() / 6 + 96, 150, 20, new LiteralChatMessage("Cancel")) {
			@Override
			public void onButtonClick(double mouseX, double mouseY) {
				Minecraft.openScreen(parent);
				onPressed.accept(false);
			}
		});
	}

	@Override
	protected void onDraw(int i, int i1, float v) {
		this.renderBackgroundTextureWrap(0);
		FontRenderer.drawCenteredString(title, this.getGuiScreenWidth() / 2, 40, 0xFFFFFF);
		int y = 90;
		for (ChatMessage line : text) {
			FontRenderer.drawCenteredString(line, this.getGuiScreenWidth() / 2, y, 0xFFFFFF);
			y += 9;
		}
	}

}
