package emc.marketplace.gui;

import me.deftware.client.framework.chat.LiteralChatMessage;
import me.deftware.client.framework.gui.GuiScreen;
import me.deftware.client.framework.gui.widgets.Button;
import me.deftware.client.framework.minecraft.Minecraft;

import java.util.function.Consumer;

/**
 * @author Deftware
 */
public class WarningScreen extends ConfirmScreen {

	public WarningScreen(GuiScreen parent, String title, Consumer<Boolean> onPressed, String... text) {
		super(parent, title, onPressed, text);
	}

	@Override
	protected void onInitGui() {
		this.clearButtons();
		this.addButton(new Button(0, this.getGuiScreenWidth() / 2 - 150, this.getGuiScreenHeight() / 6 + 96, 300, 20, new LiteralChatMessage("Ok")) {
			@Override
			public void onButtonClick(double mouseX, double mouseY) {
				Minecraft.openScreen(parent);
				onPressed.accept(true);
			}
		});
	}

}
