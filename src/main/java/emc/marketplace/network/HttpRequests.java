package emc.marketplace.network;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;

/**
 * @author Deftware
 */
public final class HttpRequests {

	public static String BROWSER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";

	public static CompletableFuture<HttpResponse> sendPostAsync(URL url, String payload, HashMap<String, String> headers, String agent) {
		return CompletableFuture.supplyAsync(() -> {
			HttpResponse response = new HttpResponse();
			try {
				response = sendPostRequest(url, payload, headers, agent);
			} catch (Exception ex){
				response.setStatusCode(500);
				response.setResponse(ex.getMessage());
			}
			return response;
		});
	}

	public static CompletableFuture<HttpResponse> sendGetAsync(URL url, HashMap<String, String> headers, String agent) {
		return CompletableFuture.supplyAsync(() -> {
			HttpResponse response = new HttpResponse();
			try {
				response = sendGetRequest(url, headers, agent, new HttpResponse());
			} catch (Exception ex){
				response.setStatusCode(500);
				response.setResponse(ex.getMessage());
			}
			return response;
		});
	}

	public static HttpResponse sendPostRequest(URL url, String payload, HashMap<String, String> headers, String agent) throws Exception {
		HttpResponse httpResponse = new HttpResponse();
		// Create connection
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setConnectTimeout(8 * 1000);
		connection.setRequestProperty("User-Agent", agent);
		if (headers != null) {
			headers.forEach(connection::setRequestProperty);
		}
		// Send it
		connection.setDoOutput(true);
		connection.addRequestProperty("Content-Type", "application/json");
		DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
		outputStream.writeBytes(payload);
		outputStream.flush();
		outputStream.close();
		connection.connect();
		// Check status
		httpResponse.fromConnection(connection);
		connection.disconnect();
		return httpResponse;
	}

	public static HttpResponse sendGetRequest(URL url, HashMap<String, String> headers, String agent, HttpResponse httpResponse) throws Exception {
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setConnectTimeout(8 * 1000);
		connection.setRequestProperty("User-Agent", agent);
		if (headers != null) {
			headers.forEach(connection::setRequestProperty);
		}
		// Get head
		connection.setRequestMethod("GET");
		connection.setInstanceFollowRedirects(true);
		// Check status
		httpResponse.fromConnection(connection);
		connection.disconnect();
		return httpResponse;
	}

}
