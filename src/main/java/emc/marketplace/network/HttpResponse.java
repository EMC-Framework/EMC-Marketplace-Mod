package emc.marketplace.network;

import lombok.Data;

import java.io.*;
import java.net.HttpURLConnection;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author Deftware
 */
public final @Data class HttpResponse {

	private int statusCode;
	private String response;

	/*
		Default types
	 */

	private Consumer<InputStream> stringHandler = stream ->
			setResponse(new BufferedReader(new InputStreamReader(stream)).lines().collect(Collectors.joining()));

	public Consumer<InputStream> fileHandler(File file) {
		return stream -> {
			try {
				FileOutputStream out = new FileOutputStream(file);
				int read;
				byte[] buffer = new byte[4096];
				while ((read = stream.read(buffer)) != -1) {
					out.write(buffer, 0, read);
				}
				stream.close();
				out.close();
				setResponse("Downloaded " + file.getName());
			} catch (Exception ex) {
				setResponse("Unable to download " + file.getName());
				ex.printStackTrace();
				if (file.exists() && !file.delete()) {
					System.err.println("Could not delete corrupt file " + file.getName());
				}
			}
		};
	}

	// Default handler is stringHandler
	private Consumer<InputStream> handler = stringHandler;

	public void fromConnection(HttpURLConnection connection) throws Exception {
		setStatusCode(connection.getResponseCode());
		if (getStatusCode() == 204) { // Error
			setResponse("No content");
		} else if (getStatusCode() == 200) {
			handler.accept(connection.getInputStream());
		} else {
			stringHandler.accept(connection.getErrorStream());
		}
	}

}

