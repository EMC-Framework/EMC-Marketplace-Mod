package emc.marketplace.api.install;

import emc.marketplace.api.SerializedMod;
import emc.marketplace.main.Main;
import emc.marketplace.network.HttpRequests;
import emc.marketplace.network.HttpResponse;
import me.deftware.client.framework.util.HashUtils;

import java.io.File;
import java.net.URI;
import java.util.List;

/**
 * @author Deftware
 */
public abstract class AbstractModMan {

	protected final File physicalFile, deleteFile, updateFile;

	protected SerializedMod parent;

	public AbstractModMan(SerializedMod parent, File physicalFile) {
		this.parent = parent;
		this.physicalFile = physicalFile;
		deleteFile = new File(physicalFile.getAbsolutePath() + ".delete");
		updateFile = new File(physicalFile.getAbsolutePath() + ".update");
	}

	/**
	 * Internal use only
	 */
	public void postInit() { }

	/**
	 * Installs the mod
	 */
	public boolean install() {
		if (physicalFile.exists() && deleteFile.exists()) {
			if (!deleteFile.delete()) {
				Main.logger.error("Could not delete {}", deleteFile.getName());
				return true;
			}
			return false;
		}
		if (!downloadUrlToFile(parent.getLink(), physicalFile)) {
			deleteFiles(null);
			return false;
		}
		return true;
	}

	protected void deleteFiles(List<File> dependencies) {
		if (physicalFile.exists() && !physicalFile.delete()) {
			Main.logger.error("Could not delete failed install file");
		}
		if (updateFile.exists() && !updateFile.delete()) {
			Main.logger.error("Could not delete failed install file");
		}
		if (deleteFile.exists() && !deleteFile.delete()) {
			Main.logger.error("Could not delete failed install file");
		}
		if (dependencies != null && !dependencies.isEmpty()) {
			for (File file : dependencies) {
				if (file.exists() && !file.delete()) {
					Main.logger.error("Could not delete failed install file");
				}
			}
		}
	}

	/**
	 * Update files should be the same file name as the mod but end with .update
	 */
	public boolean update() {
		if (updateFile.exists()) return true;
		if (!downloadUrlToFile(parent.getLink(), updateFile)) {
			if (updateFile.exists() && !updateFile.delete()) {
				Main.logger.error("Could not delete failed update file");
			}
			return false;
		}
		return true;
	}

	/**
	 * Uninstall files should be the same file name as the mod but end with .delete
	 */
	public boolean uninstall() {
		try {
			if (!deleteFile.exists() && !deleteFile.createNewFile()) {
				throw new Exception("Could not create .delete file");
			}
			if (updateFile.exists() && !updateFile.delete()) {
				throw new Exception("Could not delete .update file");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Checks if the mod is installed and doesn't have a pending uninstall
	 */
	public boolean isInstalled() {
		return physicalFile.exists() && !deleteFile.exists();
	}

	/**
	 * If the mod is supported on the current mc version
	 */
	public boolean isSupported() {
		return true;
	}

	/**
	 * Should return a sha1 of the physical jar file
	 */
	public String getLocalChecksum() {
		try {
			if (!isInstalled()) throw new Exception("Mod not installed!");
			return HashUtils.getSha1(physicalFile);
		} catch (Exception ex) {
			Main.logger.error("Failed to get checksum of {}", physicalFile.getName());
			ex.printStackTrace();
			return "0";
		}
	}

	/**
	 * Downloads a file from a url
	 */
	public static boolean downloadUrlToFile(String uri, File toFile) {
		try {
			Main.logger.debug("Downloading {} from {}", toFile.getName(), uri);
			HttpResponse response = new HttpResponse();
			response.setHandler(response.fileHandler(toFile));
			HttpRequests.sendGetRequest(new URI(uri).toURL(), null, HttpRequests.BROWSER_AGENT, response);
			Main.logger.debug("{} & {}", response.getStatusCode(), toFile.getName());
			boolean status = response.getStatusCode() == 200 && toFile.exists();
			if (!status)
				Main.logger.error("Received non 200 OK status trying to download {}", toFile.getName());
			return status;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

}
