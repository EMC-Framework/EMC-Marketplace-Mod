package emc.marketplace.api.install;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import emc.marketplace.api.SerializedMod;
import emc.marketplace.main.Main;
import me.deftware.client.framework.FrameworkConstants;
import me.deftware.client.framework.main.EMCMod;
import me.deftware.client.framework.main.bootstrap.Bootstrap;
import me.deftware.client.framework.minecraft.Minecraft;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.stream.Collectors;

/**
 * The official EMC format mod manager
 *
 * @author Deftware
 */
public class EMCModMan extends AbstractModMan {

	public EMCModMan(SerializedMod parent) {
		super(parent, new File(Minecraft.getRunDir(), "libraries" + File.separator + "EMC" + File.separator + Minecraft.getMinecraftVersion() + File.separator + parent.getId() + ".jar"));
	}

	public void postInit() {
		// Not officially part of the spec, it's a custom file used by the EMC marketplace preprocessor
		File reinstallFile = new File(physicalFile.getAbsolutePath() + ".reinstall");
		// Check for reinstall update
		if (reinstallFile.exists()) {
			Main.logger.info("Reinstalling {} ({})...", parent.getName(), parent.getId());
			if (!reinstallFile.delete()) {
				Main.logger.error("Could not delete {}", reinstallFile.getName());
				return;
			}
			parent.install(null);
		}
	}

	@Override
	public boolean install() {
		if (super.install()) {
			load();
			return true;
		}
		return false;
	}

	@Override
	public boolean uninstall() {
		if (super.uninstall()) {
			Bootstrap.getMods().get(parent.getId()).callMethod("uninstall()", "EMC-Marketplace", null);
			return true;
		}
		return false;
	}

	/**
	 * Loads the EMC mod at runtime
	 */
	private void load() {
		try {
			Main.logger.info("Attempting to load {} ({}) v{}...", parent.getName(), parent.getId(), parent.getVersion());
			String path = physicalFile.toURI().toURL().getFile();
			JarURLConnection connection = (JarURLConnection) new URL("jar", "", "file:" + path + "!/client.json").openConnection();
			// Create classLoader
			URLClassLoader classLoader = URLClassLoader.newInstance(
					new URL[]{new URL("jar", "", "file:" + path + "!/")},
					Bootstrap.class.getClassLoader());
			// Open and read json file
			BufferedReader buffer = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			JsonObject json = new Gson().fromJson(buffer.lines().collect(Collectors.joining("\n")), JsonObject.class);
			buffer.close();

			// Load main class from classLoader
			EMCMod instance = (EMCMod) classLoader.loadClass(json.get("main").getAsString()).newInstance();
			// Set instance classLoader
			instance.classLoader = classLoader;

			// Check compatibility
			String[] minVersion = (json.has("minVersion") ? json.get("minVersion").getAsString() : String.format("%s.%s", FrameworkConstants.VERSION, FrameworkConstants.PATCH)).split("\\.");
			if (Double.parseDouble(String.format("%s.%s", minVersion[0], minVersion[1])) >= FrameworkConstants.VERSION && Integer.parseInt(minVersion[2]) > FrameworkConstants.PATCH) {
				Bootstrap.logger.warn("Will not load {}, unsupported EMC version", physicalFile.getName());
				return;
			} else if (!json.has("scheme") || json.get("scheme").getAsInt() < FrameworkConstants.SCHEME) {
				Bootstrap.logger.warn("Will not load unsupported mod {}, unsupported scheme", physicalFile.getName());
				return;
			}
			// Ensure only one instance is loaded
			if (Bootstrap.getMods().containsKey(json.get("name").getAsString())) {
				return;
			}
			Bootstrap.logger.debug("Loading {} v{} by {}", json.get("name").getAsString(), json.get("version").getAsString(), json.get("author").getAsString());
			Bootstrap.getMods().put(json.get("name").getAsString(), instance);
			Bootstrap.getMods().get(json.get("name").getAsString()).init(json);
			Bootstrap.logger.info("Loaded {}", json.get("name").getAsString());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean update() {
		if (updateFile.exists()) return true;
		return download(updateFile);
	}

	private boolean download(File toFile) {
		return downloadUrlToFile(parent.getLink(), toFile);
	}

}
