package emc.marketplace.api.install;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import emc.marketplace.api.SerializedMod;
import emc.marketplace.main.Main;
import me.deftware.client.framework.FrameworkConstants;
import me.deftware.client.framework.minecraft.Minecraft;
import me.deftware.client.framework.util.path.LocationUtil;

import java.io.File;
import java.util.ArrayList;

/**
 * Manages fabric installs, automatic updates depends on Subsystem being used, instead of fabric-loader
 *
 * @author Deftware
 */
public class FabricModMan extends AbstractModMan {

	private final boolean isSupported;
	private boolean updateSupported = false;

	/**
	 * Fabric data for the relevant mc version
	 */
	private JsonObject fabricJsonData;

	public FabricModMan(SerializedMod parent) {
		super(parent, new File(System.getProperty("EMCDir", LocationUtil.getEMC().toFile().getParentFile().getAbsolutePath()) + File.separator + parent.getFabric().get("fileName").getAsString().replace("%mc%", Minecraft.getMinecraftVersion()) + ".jar"));
		boolean fabricEnv = FrameworkConstants.MAPPING_LOADER == FrameworkConstants.MappingsLoader.Fabric;
		isSupported = parent.getFabric().get("versions").getAsJsonObject().has(Minecraft.getMinecraftVersion()) && fabricEnv;
		if (isSupported) {
			fabricJsonData = parent.getFabric().get("versions").getAsJsonObject().get(Minecraft.getMinecraftVersion()).getAsJsonObject();
			parent.setLink(fabricJsonData.get("url").getAsString());
			if (fabricJsonData.has("sha1")) {
				parent.setSha1(fabricJsonData.get("sha1").getAsString());
				updateSupported = true;
			}
		} else {
			String reason = "Running in non Fabric environment";
			if (fabricEnv) {
				reason = "Unsupported Minecraft protocol version";
			}
			Main.logger.info("Ignoring unsupported Fabric mod {} (Reason: {})", parent.getName(), reason);
		}
	}

	@Override
	public boolean install() {
		if (super.install()) {
			// Check for dependencies
			if (fabricJsonData.has("depends")) {
				Main.logger.info("Found external Fabric dependencies for {}", parent.getName());
				JsonArray dependencies = fabricJsonData.getAsJsonArray("depends");
				ArrayList<File> localDependencies = new ArrayList<>();
				for (JsonElement element : dependencies) {
					JsonObject dep = element.getAsJsonObject();
					Main.logger.info("Downloading Fabric mod dependency {}", dep.get("name").getAsString());
					File file = new File(physicalFile.getParentFile().getAbsolutePath() + File.separator + dep.get("name").getAsString());
					localDependencies.add(file);
					if (!downloadUrlToFile(dep.get("url").getAsString(), file)) {
						Main.logger.error("Failed to download dependency {}", dep.get("name").getAsString());
						deleteFiles(localDependencies);
						return false;
					}
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean uninstall() {
		if (fabricJsonData.has("depends")) {
			Main.logger.info("Found external Fabric dependencies for {}", parent.getName());
			JsonArray dependencies = fabricJsonData.getAsJsonArray("depends");
			for (JsonElement element : dependencies) {
				JsonObject dep = element.getAsJsonObject();
				Main.logger.info("Uninstalling Fabric mod dependency {}", dep.get("name").getAsString());
				File file = new File(physicalFile.getParentFile().getAbsolutePath() + File.separator + dep.get("name").getAsString()),
					deleteFile = new File(file.getAbsolutePath() + ".delete");
				try {
					if (file.exists() && !deleteFile.exists() && !deleteFile.createNewFile()) {
						Main.logger.error("Could not create uninstall file");
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return super.uninstall();
	}

	@Override
	public boolean isSupported() {
		// Make sure we're running fabric
		return isSupported && FrameworkConstants.MAPPING_LOADER == FrameworkConstants.MappingsLoader.Fabric;
	}

	@Override
	public boolean update() {
		if (updateSupported) {
			return super.update();
		}
		return false;
	}

}
