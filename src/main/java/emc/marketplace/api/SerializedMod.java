package emc.marketplace.api;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import emc.marketplace.main.Main;
import emc.marketplace.api.install.AbstractModMan;
import emc.marketplace.api.install.EMCModMan;
import emc.marketplace.api.install.FabricModMan;
import lombok.Data;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * An object representation of the mod json entry
 *
 * @author Deftware
 */
public @Data class SerializedMod {

	/*
		Json data
	 */

	public @Expose @SerializedName("beta") boolean beta;

	/**
	 * Mods flagged for maintenance should be hidden
	 */
	public @Expose @SerializedName("maintenance") boolean maintenance;

	public @Expose @SerializedName("id") String id;
	public @Expose @SerializedName("name") String name;
	public @Expose @SerializedName("author") String author;
	public @Expose @SerializedName("version") String version;
	public @Expose @SerializedName("sha1") String sha1;
	public @Expose @SerializedName("link") String link;
	public @Expose @SerializedName("summary") String summary;

	public @Expose @SerializedName("depends") ArrayList<String> depends;
	public @Expose @SerializedName("description") ArrayList<String> description;

	public @Expose @SerializedName("fabric") JsonObject fabric;

	public @Expose @SerializedName("conflicts") ArrayList<String> conflicts;

	/*
		Mod data
	 */

	private AbstractModMan modMan;

	/**
	 * Checks for updates and other various tasks
	 */
	public void initialize() {
		modMan = fabric == null ? new EMCModMan(this) : new FabricModMan(this);
		modMan.postInit();
		// Check for updates
		if (!(modMan instanceof FabricModMan) && modMan.isInstalled() && hasUpdate() && !maintenance) {
			Main.logger.info("Updating {} ({})...", name, id);
			modMan.update();
		}
	}

	/**
	 * If the mod is installed or not
	 */
	public boolean isInstalled() {
		return modMan.isInstalled();
	}

	/**
	 * Uninstalls the mod
	 */
	public boolean uninstall() {
		if (!modMan.isInstalled()) return false;
		Main.logger.info("Uninstalling {} ({})...", name, id);
		return modMan.uninstall();
	}

	/**
	 * Whether or not this mod is supported in the current mc version
	 */
	public boolean isSupported() {
		return modMan.isSupported();
	}

	/**
	 * Returns an array of installed mods that conflict with this mod
	 */
	@Nullable
	public List<String> getConflicts() {
		List<String> conflictList = new ArrayList<>();
		if (conflicts != null && !conflicts.isEmpty()) {
			for (String conflict : conflicts) {
				SerializedMod modConflict = MarketplaceAPI.getModById(conflict);
				if (modConflict != null && modConflict.isInstalled()) conflictList.add(conflict);
			}
		}
		return conflictList;
	}

	/**
	 * Installs a mod and any required dependencies
	 */
	public void install(Consumer<Boolean> callback) {
		if (modMan.isInstalled()) {
			if (callback != null) callback.accept(true);
			return;
		}
		// Installs are ran on a new thread to not block the maim UI
		new Thread(() -> {
			Thread.currentThread().setName(id + " Installer");
			Main.logger.info("Installing {} ({}) v{}...", name, id, version);
			// Install any required dependencies first
			if (depends != null && !depends.isEmpty()) {
				for (String dependency : depends) {
					SerializedMod modDependency = MarketplaceAPI.getModById(dependency);
					if (modDependency != null && !modDependency.isInstalled()) {
						modDependency.install(cb -> {
							if (!cb) Main.logger.error("Failed to install dependency {} ({})", modDependency.getName(), modDependency.getId());
						});
					}
				}
			}
			// Install actual mod
			boolean result = modMan.install();
			if (callback != null) callback.accept(result);
		}).start();
	}

	/**
	 * Checks for an update based on sha1
	 */
	public boolean hasUpdate() {
		// Fabric mods cannot be updated for now
		if (modMan instanceof FabricModMan) return false;
		return !sha1.equalsIgnoreCase(modMan.getLocalChecksum());
	}

}
