package emc.marketplace.api;

import com.google.gson.*;
import com.google.gson.annotations.Expose;
import com.google.gson.reflect.TypeToken;
import me.deftware.client.framework.util.WebUtils;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Handles communication with the marketplace
 *
 * @author Deftware
 */
public class MarketplaceAPI {

    private static final String endpoint = "https://gitlab.com/EMC-Framework/maven/raw/master/marketplace/index.json";
	private static final HashMap<String, SerializedMod> cachedMods = new HashMap<>();
	private static boolean fetching = false;

	/**
	 * Returns a mod instance from its id
	 */
	@Nullable
	public static SerializedMod getModById(String id) {
		return cachedMods.getOrDefault(id, null);
	}

	/**
	 * Returns the total amount of cached mods
	 */
	public static int getTotalMods() {
		if (fetching || cachedMods.isEmpty()) return 0;
		return cachedMods.size();
	}

	/**
	 * Returns a stream of all cached mods
	 */
	@Nullable
	public static Stream<SerializedMod> getMods() {
		if (fetching) return null;
		return !cachedMods.isEmpty() ? cachedMods.values().stream() : null;
	}

	/**
	 * Updates the mod cache
	 */
	public static void updateCache(Consumer<Boolean> callback) {
		// Run on new thread to not block the UI
		new Thread(() -> {
			Thread.currentThread().setName("Mod fetcher");
			fetching = true;
			try {
				// Fetch and serialize the mods
				JsonObject json = new Gson().fromJson(WebUtils.get(endpoint, null), JsonObject.class);
				ExclusionStrategy strategy = new ExclusionStrategy() {

					@Override
					public boolean shouldSkipClass(Class<?> clazz) {
						return false;
					}

					@Override
					public boolean shouldSkipField(FieldAttributes field) {
						return field.getAnnotation(Expose.class) == null;
					}

				};
				List<SerializedMod> mods = new GsonBuilder().addDeserializationExclusionStrategy(strategy).addSerializationExclusionStrategy(strategy).create().fromJson(json.get("mods").getAsJsonArray(), new TypeToken<List<SerializedMod>>(){}.getType());
				// Update the cache
				cachedMods.clear();
				for (SerializedMod mod : mods) {
					if (!mod.isMaintenance()) {
						mod.initialize();
						if (mod.isSupported()) {
							cachedMods.put(mod.getId(), mod);
						}
					}
				}
				fetching = false;
				callback.accept(true);
			} catch (Exception ex) {
				ex.printStackTrace();
				callback.accept(false);
			}
		}).start();
	}

}
