package emc.marketplace.main;

import emc.marketplace.gui.ModListScreen;
import me.deftware.client.framework.chat.LiteralChatMessage;
import me.deftware.client.framework.event.EventHandler;
import me.deftware.client.framework.event.EventListener;
import me.deftware.client.framework.event.events.EventGuiScreenDraw;
import me.deftware.client.framework.global.GameKeys;
import me.deftware.client.framework.global.GameMap;
import me.deftware.client.framework.gui.minecraft.ScreenInstance;
import me.deftware.client.framework.gui.widgets.Button;
import me.deftware.client.framework.minecraft.Minecraft;

/**
 * @author Deftware
 */
class EventManager extends EventListener {

    @EventHandler
    private void drawButton(EventGuiScreenDraw event) {
        if (event.getInstance().getType() == ScreenInstance.CommonScreenTypes.GuiIngameMenu && event.getEmcButtons().size() <= 3) {
            if (GameMap.INSTANCE.get(GameKeys.MARKETPLACE_ESC_BUTTON, true)) {
                event.addButton(new Button(30, event.getWidth() / 2 + 4, event.getHeight() / 4 + 152, 98, 20, new LiteralChatMessage("Addons")) {
                    @Override
                    public void onButtonClick(double mouseX, double mouseY) {
                        Minecraft.openScreen(new ModListScreen(null));
                    }
                });
            }
        }
    }

}
