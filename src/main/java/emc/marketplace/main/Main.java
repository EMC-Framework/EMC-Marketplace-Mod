package emc.marketplace.main;

import emc.marketplace.gui.ModListScreen;
import emc.marketplace.api.MarketplaceAPI;
import me.deftware.client.framework.FrameworkConstants;
import me.deftware.client.framework.gui.GuiScreen;
import me.deftware.client.framework.main.EMCMod;
import me.deftware.client.framework.minecraft.Minecraft;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Deftware
 */
public class Main extends EMCMod {

    public static final Logger logger = LogManager.getLogger("EMC-Marketplace");
    public static String status = "";
    public static int version;
    public static Main INSTANCE;

    @Override
    public void initialize() {
        INSTANCE = this;
        if (FrameworkConstants.SCHEME >= 4) {
            version = modInfo.get("version").getAsInt();
            new EventManager();
            long start = System.currentTimeMillis();
            MarketplaceAPI.updateCache(cb -> {
                if (cb) {
                    Main.logger.info("Fetched {} mods in {}ms", MarketplaceAPI.getTotalMods(), System.currentTimeMillis() - start);
                } else {
                    Main.logger.error("Could not fetch mods");
                }
            });
        }
    }

    @Override
    public void callMethod(String method, String caller, Object parent) {
        if (method.equals("openGUI()")) {
            Minecraft.openScreen(new ModListScreen((GuiScreen) parent));
        } else if (method.startsWith("setStatus(")) {
            status = method.substring("setStatus(".length(), method.length() - 1);
        }
    }

}
